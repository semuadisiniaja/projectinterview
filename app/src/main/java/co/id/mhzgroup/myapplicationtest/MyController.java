package co.id.mhzgroup.myapplicationtest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MyController extends DatabaseHandler {
    String TAG = "MyController";

    public MyController(Context context) {
        super(context);
    }

    public Cursor getListProduct() {
        Log.w(TAG, "getListProduct()");
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from Barang", null);
        return c;
    }

    public void insertProduct(String namaBarang, String qty, String expDate, String harga) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_NAMA_BARANG,namaBarang);
        contentValues.put(COLUMN_QTY,Integer.parseInt(qty));
        contentValues.put(COLUMN_EXP_DATE,expDate);
        contentValues.put(COLUMN_HARGA,Integer.parseInt(harga));

        db.insert(TABLE_BARANG,null,contentValues);

    }


}
