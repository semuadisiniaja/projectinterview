package co.id.mhzgroup.myapplicationtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    Button btnLogin;
    AutoCompleteTextView inputPassword;
    EditText inputUsername;

    private DBsource dBsource;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dBsource = new DBsource(this);
        db = new DatabaseHandler(this);
        dBsource.open();

        inputUsername = (EditText) findViewById(R.id.input_username);
        inputPassword = (AutoCompleteTextView) findViewById(R.id.input_password);

        btnLogin = (Button) findViewById(R.id.btn_login);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inputUsername.getText().toString().equals("admin")
                        &&inputPassword.getText().toString().equals("12345")){
                    Intent intent = new Intent(MainActivity.this, ListProduct.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(MainActivity.this, "Username/Password tidak sesuai", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}