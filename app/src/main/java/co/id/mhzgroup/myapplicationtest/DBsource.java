package co.id.mhzgroup.myapplicationtest;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBsource {
    private SQLiteDatabase database;
    private DatabaseHandler dbHelper;

    public DBsource (Context context) {
        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        Log.w("OPEN()","DBSource");
        database = dbHelper.getWritableDatabase();
    }

}
