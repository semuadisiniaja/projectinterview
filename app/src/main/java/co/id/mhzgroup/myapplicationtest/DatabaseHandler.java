package co.id.mhzgroup.myapplicationtest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static String TAG = DatabaseHandler.class.getSimpleName() + "Class ";
    private Context context;
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DBNAME";

    public static final String TABLE_BARANG = "Barang";
    public static final String COLUMN_ID = "Id";
    public static final String COLUMN_NAMA_BARANG = "Nama_Barang";
    public static final String COLUMN_QTY = "Qty";
    public static final String COLUMN_EXP_DATE = "Exp_Date";
    public static final String COLUMN_HARGA = "Harga";

    //Constructor
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //creating the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TRN_PROJECT = "CREATE TABLE " + TABLE_BARANG + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY, "
                + COLUMN_NAMA_BARANG + " TEXT,"
                + COLUMN_QTY + " INTEGER, "
                + COLUMN_EXP_DATE + " DATETIME, "
                + COLUMN_HARGA+ " INTEGER" + ")";
        db.execSQL(CREATE_TRN_PROJECT);
        Log.i(TAG, "Success created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("onUpgrade", "onUpgrade");
        String sql = "DROP TABLE IF EXISTS Persons";
        db.execSQL(sql);
        onCreate(db);

    }

}
