package co.id.mhzgroup.myapplicationtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class FormProduct extends AppCompatActivity {

    EditText inputNama,inputQty,inputExpDate,inputHarga;
    Button btnSave;

    MyController myController = new MyController(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_product);
        setTitle("Buat Produk Baruu");

        inputNama = (EditText) findViewById(R.id.inputNamaBarang);
        inputQty = (EditText) findViewById(R.id.inputQty);
        inputExpDate = (EditText) findViewById(R.id.inputExpDate);
        inputHarga = (EditText) findViewById(R.id.inputHarga);

        btnSave = (Button) findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w("FORM","Waktu habis saat mau save, maaf apabila ada error");
                myController.insertProduct(
                        inputNama.getText().toString(),
                        inputQty.getText().toString(),
                        inputExpDate.getText().toString(),
                        inputHarga.getText().toString()
                );
            }
        });

    }
}